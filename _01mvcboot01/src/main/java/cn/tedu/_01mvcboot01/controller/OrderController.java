package cn.tedu._01mvcboot01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {
    /**
     * 1.RequestMapping:请求注解，根据请求的URL地址的路径映射到对应的控制方法
     * 2.ResponseBody:响应注解，将控制方法的返回值作为响应内容发送给客户端
     * 浏览器测试:http://localhost:8080/cs/cf/csgo
     */
    @RequestMapping("cs/cf/csgo")
    @ResponseBody
    public String selectOrder()
    {
        return "你真牛逼,查询成功";
    }
}
