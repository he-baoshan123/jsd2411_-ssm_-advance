package cn.tedu._01mvcboot01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CartsController {
    @RequestMapping("/v1/carts/add")
    @ResponseBody
    public String addcarts() {
        return "添加购物车成功";
    }
    @RequestMapping("/v1/carts/list")
    @ResponseBody
    public String listCarts() {
        return "查询购物车成功";
    }
}
