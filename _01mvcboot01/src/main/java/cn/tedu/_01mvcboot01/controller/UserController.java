package cn.tedu._01mvcboot01.controller;

import cn.tedu._01mvcboot01.pojo.dto.UserLoginDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    @GetMapping("v1/user/login1")
    @ResponseBody
    public String login1(String username, String password){
        System.out.println("用户名：" + username + "，密码：" + password);
        return "登录成功1";
    }

    @PostMapping("v1/user/login2")
    @ResponseBody
    public String login2(UserLoginDTO userLoginDTO){
        System.out.println(userLoginDTO);
        return "登录成功";
    }

    @GetMapping("/bmi/{height}/{weight}")
    @ResponseBody
    public String bmi(@PathVariable Double height, @PathVariable Double weight){
        double r = weight / (height * height);
        if (r < 18.5){
            return "细狗";
        }else if (r < 24.9){
            return "正常";
        }else if (r < 29){
            return "小胖";
        }else {
            return "肥仔";
        }
        }
    }


