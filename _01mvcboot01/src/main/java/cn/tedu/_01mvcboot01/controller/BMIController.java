package cn.tedu._01mvcboot01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BMIController {

    /**
     * GET测试[浏览器中]：http://localhost:8080/bmi?height=1.75&weight=40
     * POST测试:
     * @param height
     * @param weight
     * @return
     */
    @RequestMapping("/bmi")
    @ResponseBody
    public String bmi(Double height, Double weight){
        System.out.println("height:"+height+" weight:"+weight);
        double r = weight / (height * height);
        if (r < 18.5){
            return "兄弟你瘦了";
        }
        if (r < 24) {
            return "正常";
        }
        if (r < 27) {
            return "微胖";
        }
        return "该减肥了";
    }
}
