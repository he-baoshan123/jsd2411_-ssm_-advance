package cn.tedu._02notice.pojo.dto;

import lombok.Data;

@Data
public class NoticeAddParam {
    //标题、类型、内容、状态
    private String title;
    private Integer type;
    private String content;
    private Integer status;
}
