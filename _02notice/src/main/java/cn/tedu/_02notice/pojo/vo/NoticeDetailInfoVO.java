package cn.tedu._02notice.pojo.vo;

import lombok.Data;

import java.util.Date;

@Data
public class NoticeDetailInfoVO {
    private Long id;
    private String title;
    private String content;
    private Integer type;
    private Long userId;
    private Date createTime;
    private Integer status;
}
