package cn.tedu._02notice.pojo.dto;

import lombok.Data;

@Data
public class NoticeListQuery {
    //标题、类型
    private String title;
    private Integer type;
}
