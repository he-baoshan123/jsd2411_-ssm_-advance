package cn.tedu._02notice.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Notice {
    private Long id;
    private String title;
    private String content;
    private Integer type;
    private Long userId;
    private Date createTime;
    private Date updateTime;
    private Integer status;
}
