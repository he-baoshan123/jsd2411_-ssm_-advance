package cn.tedu._02notice.pojo.dto;

import lombok.Data;

@Data
public class NoticeUpdateParam {
    private Long id;
    private String title;
    private String content;
    private Integer type;
    private Integer status;
}
