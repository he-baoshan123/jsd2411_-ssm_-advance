package cn.tedu._02notice.mapper;

import cn.tedu._02notice.pojo.entity.Notice;
import cn.tedu._02notice.pojo.vo.NoticeDetailInfoVO;
import cn.tedu._02notice.pojo.vo.NoticeListVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

//@Mapper
public interface NoticeMapper {
    /**
     * 1.新增公告接口方法
     * @param notice 实体类
     * @return 受影响的数据条数
     */
    int insertNotice(Notice notice);

    /**
     * 2.公告列表接口
     * @param title 公告标题
     * @param type 公告类型
     * @return List<>集合
     */
    List<NoticeListVO> selectNotice(@Param("title") String title,
                                    @Param("type") Integer type);

    /**
     * 3.公告详情接口
     * @param id 公告id
     * @return 公告详情VO
     */
    NoticeDetailInfoVO detail(Long id);

    /**
     * 4.删除公告接口
     * @param id 公告id
     * @return 受影响的数据条数
     */
    int delNotice(Long id);

    /**
     * 5.更新公告接口
     * @param notice 实体类
     * @return 受影响的数据条数
     */
    int updateNotice(Notice notice);
}
