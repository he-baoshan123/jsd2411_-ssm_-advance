package cn.tedu._02notice.controller;

import cn.tedu._02notice.mapper.NoticeMapper;
import cn.tedu._02notice.pojo.dto.NoticeAddParam;
import cn.tedu._02notice.pojo.dto.NoticeListQuery;
import cn.tedu._02notice.pojo.dto.NoticeUpdateParam;
import cn.tedu._02notice.pojo.entity.Notice;
import cn.tedu._02notice.pojo.vo.NoticeDetailInfoVO;
import cn.tedu._02notice.pojo.vo.NoticeListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * RestController注解:
 *   1.组合注解,等价于Controller注解 + ResponseBody注解;
 *   2.加完该注解后,所有的控制器方法上无需再添加 @ResponseBody注解 了;
 */
@RestController
@RequestMapping("/v1/notice/")
public class NoticeController {
    //自动装配
    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 1.新增公告功能
     */
    @PostMapping("add")
    public String addNotice(NoticeAddParam noticeAddParam){
        /*
            梳理业务逻辑,确定接口;
            1.接收客户端传递的数据;
            2.校验数据的合法性【暂时不校验】;
            3.调用mapper层插入数据[INSERT接口];
            4.返回响应;

            以注册用户业务为例:
            1.接收数据;
            2.校验合法性;
            3.确定用户名是否被占用[SELECT接口];
              3.1 被占用:return结束;
              3.2 未被占用: 插入数据[INSERT接口],返回;

           完成一个功能的流程步骤:
           1.确定是否需要DTO或者VO;
           2.梳理业务逻辑,确定mapper层接口,完成mapper层接口;
           3.完成Controller中的业务逻辑;
           4.重启工程测试;
         */
        System.out.println("noticeAddParam:" + noticeAddParam);
        Notice notice = new Notice();
        //复制属性
        BeanUtils.copyProperties(noticeAddParam, notice);
        notice.setUserId(1L);
        notice.setCreateTime(new Date());
        notice.setUpdateTime(new Date());
        //调用mapper层接口
        noticeMapper.insertNotice(notice);
        return "新增公告成功";
    }

    /**
     * 2.公告列表功能
     */
    @GetMapping("list")
    public List<NoticeListVO> noticeList(NoticeListQuery noticeListQuery){
        System.out.println("noticeListQuery:" + noticeListQuery);
        String title = noticeListQuery.getTitle();
        Integer type = noticeListQuery.getType();
        //直接调用mapper层接口
        List<NoticeListVO> noticeListVOS = noticeMapper.selectNotice(title, type);

        return noticeListVOS;
    }

    /**
     * 3.公告详情功能
     *   http://localhost:8080/v1/notice/detail?id=1
     */
    @GetMapping("detail")
    public NoticeDetailInfoVO detail(Long id){
        System.out.println("id = " + id);
        NoticeDetailInfoVO noticeDetailInfoVO = noticeMapper.detail(id);

        return noticeDetailInfoVO;
    }

    /**
     * 4.删除公告功能
     */
    @PostMapping("del")
    public String delNotice(Long id){
        System.out.println("id = " + id);
        noticeMapper.delNotice(id);

        return "删除公告成功";
    }

    /**
     * 5.更新公告功能
     */
    @PostMapping("update")
    public String updateNotice(NoticeUpdateParam noticeUpdateParam){
        System.out.println("noticeUpdateParam:" + noticeUpdateParam);
        Notice notice = new Notice();
        BeanUtils.copyProperties(noticeUpdateParam, notice);
        notice.setUpdateTime(new Date());//因为是更新功能,所以需要设置更新时间
        noticeMapper.updateNotice(notice);

        return "更新公告成功";
    }
}










