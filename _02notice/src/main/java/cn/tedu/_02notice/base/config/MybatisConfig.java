package cn.tedu._02notice.base.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 1.复制路径快捷键: Ctrl + Shift + Alt + c
 * 2.Configuration注解:该类为配置类,工程启动时会优先加载此类;
 * 3.MapperScan注解:自动扫描注解;
 *   3.1 自动扫描指定包及子孙包中的所有的接口,为其添加@Mapper注解;
 *   3.2 设置后mapper包下的所有接口均不需要再添加@Mapper注解了;
 */
@Configuration
@MapperScan("cn.tedu._02notice.mapper")
public class MybatisConfig {
}
