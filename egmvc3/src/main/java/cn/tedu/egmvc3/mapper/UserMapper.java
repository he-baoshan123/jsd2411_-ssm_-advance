package cn.tedu.egmvc3.mapper;

import cn.tedu.egmvc3.pojo.entity.User;
import cn.tedu.egmvc3.pojo.vo.UserListVO;

import java.util.List;

public interface UserMapper {
    int addUser(User user);

    List<UserListVO> userList();

    int delUser(Integer id);

    int updateUser(User user);
}
