package cn.tedu.egmvc3.base.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.egmvc3.mapper")
public class MybatisConfig {
}
