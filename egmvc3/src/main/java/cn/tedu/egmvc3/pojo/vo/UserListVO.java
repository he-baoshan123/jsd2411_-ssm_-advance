package cn.tedu.egmvc3.pojo.vo;

import lombok.Data;

@Data
public class UserListVO {
    private String username;
    private String password;
}
