package cn.tedu.egmvc3.pojo.vo;

import lombok.Data;

@Data
public class UserAddParam {
    private String username;
    private String password;
    private String nickname;
}
