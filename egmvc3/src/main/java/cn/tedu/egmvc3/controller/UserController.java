package cn.tedu.egmvc3.controller;

import cn.tedu.egmvc3.mapper.UserMapper;
import cn.tedu.egmvc3.pojo.entity.User;
import cn.tedu.egmvc3.pojo.vo.UserAddParam;
import cn.tedu.egmvc3.pojo.vo.UserListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/user/")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    /**
     * 1.添加用户
     * @param userAddParam
     * @return
     */
    @PostMapping("add")
    public String addUser(UserAddParam userAddParam) {
        System.out.println("userAddParam = " + userAddParam);
        User user = new User();
        BeanUtils.copyProperties(userAddParam, user);
        user.setCreateTime(new Date());
        userMapper.addUser(user);

        return "添加用户成功";
    }

    /**
     * 2.用户列表
     * @return
     */
    @GetMapping("list")
    public List<UserListVO> userList() {
        return userMapper.userList();
    }

    /**
     * 3.删除指定用户
     */
    @PostMapping("del")
    public String delUser(Integer id) {
        System.out.println("id = " + id);
        userMapper.delUser(id);

        return "删除成功";
    }

    /**
     * 4.更新指定用户
     */
    @PostMapping("update")
    public String updateUser(User user){
        System.out.println("user = " + user);
        userMapper.updateUser(user);

        return "修改成功";
    }
}








